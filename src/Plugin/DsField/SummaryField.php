<?php
namespace Drupal\ds_extra_fields\Plugin\DsField;
use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\ds\Plugin\DsField\DsFieldBase;
#use Drupal\taxonomy\Entity\Term;
#use Drupal\taxonomy\Entity\Vocabulary;
/**
 * Plugin that renders summary field
 *
 * @DsField(
 *   id = "summary_field",
 *   title = @Translation("Body summary"),
 *   entity_type = "node",
 *   provider = "ds_extra_fields"
 * )
 */
class SummaryField extends DsFieldBase {
  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();

    $node = $this->entity();

    if (isset($node->body)) {
      $summary = $this->formatText($node->get('body')->summary);
      $format = $node->get('body')->format;
    }

    if (empty($summary)) {
      return;
    }

    return array(
      '#type' => 'processed_text',
      '#text' => $summary,
      '#format' => $format
    );
  }

  /**
   * {@inheritdoc}
   */
  private function formatText($item) {
    
    $config = $this->getConfiguration();
    $formatter = isset($config['field']['formatter']) && $config['field']['formatter'] ? $config['field']['formatter'] : 'html';
    
    if ($formatter === 'strip') {
      $item = '<p>' . strip_tags($item) . '</p>';
    } else {
      $item = $this->nl2p($item);
    }

    return $item;
    
  }

  /**
   * {@inheritdoc}
   */
  private function nl2p($string) {
    $paragraphs = '';

    foreach (explode("\n", $string) as $line) {
        if (trim($line)) {
            $paragraphs .= '<p>' . $line . '</p>';
        }
    }

    return $paragraphs;
  }

  /**
   * {@inheritdoc}
   */
  public function formatters() {
    return array('html' => 'Allow HTML', 'strip' => 'Strip all HTML');
  }
}